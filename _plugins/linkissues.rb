Jekyll::Hooks.register([:pages, :posts], :post_render) do |post|
  post.output = post.output.gsub(/(i|I)ssue #([0-9]+)/, '\1ssue <a href="//tortoisegit.org/issue/\2" target="_blank">#\2</a>')
end
